<?php
namespace App\Model;

use Core\Kernel\AbstractModel;
use Core\App;

class UserModel extends AbstractModel
{
    protected static $table = 'user';

    protected $id;
    protected $nom;
    protected $email;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }



    public static function insert($post)
    {
        App::getDatabase()->prepareInsert(
            "INSERT INTO " . self::$table . " (email, nom ) VALUES (?,?)",
            array($post['email'], $post['nom'])
        );
    }






}