<?php


namespace App\Model;

use Core\Kernel\AbstractModel;
use Core\App;

class CreneauxModel extends AbstractModel
{
    protected static $table = 'creneau';

    protected $id;
    protected $id_salle;
    protected $start_at;
    protected $nbrehours;
    protected $title;
    protected $max_user;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getSalle()
    {
        return $this->title;
    }




    /**
     * @return mixed
     */
    public function getIdSalle()
    {
        return $this->id_salle;
    }

    /**
     * @return mixed
     */
    public function getStartAt()
    {
        return $this->start_at;
    }

    /**
     * @return mixed
     */
    public function getNbrehours()
    {
        return $this->nbrehours;
    }





    public static function insert($post)
    {
        App::getDatabase()->prepareInsert(
            "INSERT INTO " . self::$table . " (id_salle, start_at, nbrehours  ) VALUES (?,?,?)",
            array($post['select-salle'], $post['start'],$post['nbrehours'])
        );
    }


    public static function getCreneau()
    {
        return App::getDatabase()->query("
        SELECT c.id, c.start_at, c.nbrehours, c.id_salle, a.title, a.max_user, a.id  
FROM " . self::$table . " AS c
LEFT JOIN salle AS a ON a.id = c.id_salle
    
     ",
            get_called_class()
        );
    }

    public static function getCreneauAll()
    {
        return App::getDatabase()->query("
        SELECT c.id, c.start_at, c.nbrehours, c.id_salle, a.title, a.max_user, a.id  
    FROM " . self::$table . " AS c
    LEFT JOIN salle AS a ON a.id = c.id_salle
    LEFT JOIN user AS u ON u.id = c.id_salle
    
     ",
            get_called_class()
        );
    }







}