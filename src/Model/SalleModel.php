<?php

namespace App\Model;

use Core\Kernel\AbstractModel;
use Core\App;

class SalleModel extends AbstractModel
{
    protected static $table = 'salle';

    protected $id;
    protected $title;
    protected $max_user;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getSalle()
    {
        return $this->title;
    }


    /**
     * @return mixed
     */
    public function getMaxuser()
    {
        return $this->max_user;
    }



    public static function insert($post)
    {
        App::getDatabase()->prepareInsert(
            "INSERT INTO " . self::$table . " (title, max_user ) VALUES (?,?)",
            array($post['titre'], $post['nombre'])
        );
    }






}