<?php

namespace App\Controller;

use App\Model\UserModel;
use App\Service\Form;
use Core\Kernel\AbstractController;
use Core\Kernel\AbstractModel;
use App\Service\Validation;


class UserController extends AbstractController
{
   public function user(){
        $errors = array();
        if(!empty($_POST['submitted'])) {
            $post = $this->cleanXss($_POST);
            // Validation
            $v = new Validation();
            $errors = $this->validate($v, $post);
            if ($v->isValid($errors)) {
                UserModel::insert($post);
                 //Message flash
                $this->addFlash('success', 'Merci message à bien étais envoyée !');
                $this->redirect('user');
            }

        }
       $form = new Form($errors);
       $this->render('app.user.user',array(
           'form' => $form,
       ));



}
    // Gestion des erreurs
    private function validate($v,$post)
    {
        $errors = [];
        $errors['nom'] = $v->textValid($post['nom'], 'nom',1, 100);
        $errors['email'] = $v->emailValid($post['email']);
        return $errors;
    }
}