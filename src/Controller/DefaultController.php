<?php

namespace App\Controller;

use App\Model\CreneauxModel;
use App\Model\SalleModel;
use App\Model\UserModel;
use Core\Kernel\AbstractController;

/**
 *
 */
class DefaultController extends AbstractController
{
    public function index()
    {



        $creneauxx = CreneauxModel::getCreneau();
        $this->dump($creneauxx);
//        $salle_id = SalleModel::findById();
//
//        $creneauxx = CreneauxModel::getCreneau($salle_id->id);

        $salles = SalleModel::all();
        $users = UserModel::all();
        $creneaux = CreneauxModel::all();
        $message = 'Bienvenue sur le framework MVC';
        //$this->dump($message);
        $this->render('app.default.frontpage',array(
            'creneaux' => $creneaux,
            'users' => $users,
            'salles' => $salles,
            'creneauxx' => $creneauxx,
        ));
    }

    public function details($id){
        $crr = $this->getcreneauxByIdOr404($id);
        $crr = SalleModel::findById($id);
        $this->dump($crr);
        $this->render('app.default.single',array(
     'id' => $crr
        ));

    }


    private function getcreneauxByIdOr404($id)
    {
        $cr = SalleModel::findById($id);
        if(empty($cr)) {
            $this->Abort404();
        }
        return $cr;
    }

    /**
     * Ne pas enlever
     */
    public function Page404()
    {
        $this->render('app.default.404');
    }
}
