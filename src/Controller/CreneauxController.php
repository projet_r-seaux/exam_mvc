<?php

namespace App\Controller;

use App\Model\CreneauxModel;
use App\Model\SalleModel;
use App\Service\Form;
use Core\Kernel\AbstractController;
use Core\Kernel\AbstractModel;
use App\Service\Validation;


class CreneauxController extends AbstractController
{
    public function creneaux(){
        $errors = array();
        $salle = SalleModel::all();

        if(!empty($_POST['submitted'])) {
            $post = $this->cleanXss($_POST);
            // Validation
            $v = new Validation();
            $errors = $this->validate($v, $post);
            if ($v->isValid($errors)) {
                CreneauxModel::insert($post);
                // Message flash
//                $this->addFlash('success', 'Merci message à bien étais envoyée !');
//                $this->redirect('user');
            }

        }
        $this->dump($errors);
        $form = new Form($errors);
        $this->render('app.creneaux.creneaux',array(
            'form' => $form,
            'salle' =>   $salle,

    ));



    }
    // Gestion des erreurs
    private function validate($v,$post)
    {
        $errors = [];
        $errors['start'] = $v->checkStartHour($post['start']);
        $errors['nbrehours'] = $v->isInteger($post['nbrehours']);
        return $errors;
    }
}
