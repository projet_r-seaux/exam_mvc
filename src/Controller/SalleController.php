<?php

namespace App\Controller;

use App\Model\SalleModel;
use App\Model\UserModel;
use App\Service\Form;
use Core\Kernel\AbstractController;
use Core\Kernel\AbstractModel;
use App\Service\Validation;

class SalleController extends AbstractController
{
    public function salle(){
        $errors = array();

        if(!empty($_POST['submitted'])) {
            $post = $this->cleanXss($_POST);
            // Validation
            $v = new Validation();
            $errors = $this->validate($v, $post);
            if ($v->isValid($errors)) {
                SalleModel::insert($post);
                // Message flash
                $this->addFlash('success', 'Merci message à bien étais envoyée !');
                $this->redirect('salle');
            }

        }
        $form = new Form($errors);
        $this->render('app.salle.salle',array(
            'form' => $form,
        ));



    }
    // Gestion des erreurs
    private function validate($v,$post)
    {
        $errors = [];
        $errors['titre'] = $v->textValid($post['titre'], 'titre',1, 100);
        $errors['nombre'] = $v->isInteger($post['nombre']);
        return $errors;
    }
}