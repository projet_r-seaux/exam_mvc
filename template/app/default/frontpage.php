
<h1 style="text-align: center;font-size:33px;margin: 100px 0;color:#A67153;">
    <div class="listing">
    <div class="wrap_lcontact">
        <h4> Liste de Users </h4>

        <table>
            <thead>
            <tr>
                <th>Email</th>
                <th>Nom</th>
            </tr>
            </thead>
            <tbody>
            <?php
            // boucle sur la liste des contact
            foreach ($users as $user) {
                $email = $user->getEmail();
                $nom = $user->getNom();

                // affiche une ligne pour chaque contact
                echo "<tr>
                        <td>$email </td>
                        <td>$nom   </td>
                      </tr>";
            }
            ?>
            </tbody>
        </table>
    </div>

        <div class="wrap_lcontact">
            <h4> Liste des salles </h4>

            <table>
                <thead>
                <tr>
                    <th>Nom de la Salle </th>
                    <th>Nombre utilisateurs de la salle </th>
                </tr>
                </thead>
                <tbody>
                <?php
                // boucle sur la liste des contact
                foreach ($salles as $salle) {
                    $titre = $salle->getSalle();
                    $max_user = $salle->getMaxuser();

                    // affiche une ligne pour chaque contact
                    echo "<tr>
                        <td>$titre </td>
                        <td>$max_user   </td>
                      </tr>";
                }
                ?>
                </tbody>
            </table>
        </div>


        <div class="wrap_lcontact">
            <h4> Liste des creneaux</h4>

            <table>
                <thead>
                <tr>
                    <th>Numero de la Salle </th>
                    <th>L'heure de commencement</th>
                    <th>La durée </th>
                    <th> Details </th>
                </tr>
                </thead>
                <tbody>
                <?php
                // boucle sur la liste des contact
                foreach ($creneauxx as $creneau) {
                    $id_salle = $creneau-> getSalle();
                    $heure = $creneau->getStartAt();
                    $duree = $creneau->getNbrehours();

                    // affiche une ligne pour chaque contact
                    echo "<tr>
              <td>$id_salle </td>
              <td>$heure Heure </td>
              <td>$duree Heure </td>
              <td><a class='btn' href='" . $view->path('details' , array('id' => $creneau->id)) . "'>Details</a></td>
          </tr>";
                }
                ?>
                </tbody>
            </table>
        </div>

</div>